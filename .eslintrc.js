module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  // add your custom rules here
  rules: {
    "camelcase": ["error", {"allow": ["jivo_api", "send_to", "ecomm_pagetype", "ecomm_prodid", "shop_name", "last_page", "have_own_delivery", "car_name", "car_number", "from_bound", "to_bound", "city_type_full", "is_not", "profile_type", "ads_type", "social_id_vk", "social_id_facebook", "social_id_instagram", "radius_meters"]}],
    "no-unused-vars" : "off"
  }
}
