export default {
  async get ({ commit, rootState }) {
    await this.$axios.$get('/api/widgets')
      .then((res) => {
        commit('set', res)
      })
  }
}
