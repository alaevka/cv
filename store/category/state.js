export default () => ({
  currentCategory: null,
  filteredProducts: [],
  pagination: {
    links: null,
    meta: {
      total: null,
      last_page: 0
    }
  },
  filter: {
    categoryId: null
  },
  sort: {
    field: { id: 'popular', label: 'Популярные' }
    // direction: 'asc'
  }
})
