export default {
  setCategory (state, data) {
    state.currentCategory = data
  },
  setProducts (state, data) {
    state.filteredProducts = data
    // const products = [...state.productsList]
    // state.filteredProductsList = products
    // state.filteredProductsList = ProductsFilterHelperComponent.filterProducts(state.filter, products)
  },
  setPaginationLinks (state, links) {
    state.pagination.links = links
  },
  setPaginationMeta (state, meta) {
    state.pagination.meta = meta
  },
  setCategoryIdFilter (state, categoryId) {
    state.filter.categoryId = categoryId
  },
  resetMeta (state) {
    state.pagination = {
      links: null,
      meta: {
        total: null,
        last_page: 0
      }
    }
  },
  setSort (state, field) {
    state.sort.field = field
  }
}
