export default {
  getCategory ({ commit }, pathMatch) {
    return new Promise((resolve, reject) => {
      this.$axios.get(`api/categories/${pathMatch}`)
        .then((res) => {
          commit('setCategory', res.data.data)
          resolve(res)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  async setFilter ({ commit, dispatch }, sort) {
    await commit('setSort', sort)
    dispatch('getProducts', 1)
  },
  async getProducts ({ commit, state, dispatch }, pageNum) {
    dispatch('wait/start', 'loading category products list', { root: true })
    await this.$axios.$post('/api/product/bycategory?page=' + pageNum, { filter: state.filter, sort: state.sort })
      .then((res) => {
        commit('setProducts', res.data)
        commit('setPaginationLinks', res.links)
        commit('setPaginationMeta', res.meta)
        dispatch('wait/end', 'loading category products list', { root: true })
      })
  }
}
