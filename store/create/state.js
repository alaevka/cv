export default () => ({
  subjectsList: [],
  activity: {
    tmpId: null,
    lastUpdatedAt: null,
    location: null,
    subject: null,
    audience: null,
    organizerExperience: '',
    activityName: '',
    clientExperienceShort: '',
    clientExperienceFull: '',
    activityDuration: '',
    activityPeopleCount: '',
    price: '',
    media: {
      mainImage: null,
      gallery: null
    }
  },
  activityDatesList: [],
  activityDate: {
    date: null,
    time: null,
    startPointDescription: '',
    lat: null,
    lon: null
  }
})
