export default {
  setActivityLocation (state, location) {
    state.activity.location = location
  },
  setActivitySubjects (state, subjects) {
    state.subjectsList = subjects
  },
  setActivityAudience (state, audience) {
    state.activity.audience = audience
  },
  setActivitySubject (state, subject) {
    state.activity.subject = subject
  },
  setActivityOrganizerExperience (state, organizerExperience) {
    state.activity.organizerExperience = organizerExperience
  },
  setActivityName (state, name) {
    state.activity.activityName = name
  },
  setActivityClientExperienceShort (state, val) {
    state.activity.clientExperienceShort = val
  },
  setActivityClientExperienceFull (state, val) {
    state.activity.clientExperienceFull = val
  },
  setActivityDuration (state, val) {
    state.activity.activityDuration = val
  },
  setActivityPeopleCount (state, val) {
    state.activity.activityPeopleCount = val
  },
  setActivityPrice (state, val) {
    state.activity.price = val
  },
  setActivityTmpId (state, val) {
    state.activity.tmpId = val
  },
  setActivityLastUpdatedAt (state, val) {
    state.activity.lastUpdatedAt = val
  },
  setCurrentActivityMedia (state, data) {
    state.activity.media = data
  },
  setCurrentActivityToNull (state) {
    state.activity = {
      tmpId: null,
      lastUpdatedAt: null,
      location: null,
      subject: null,
      audience: null,
      organizerExperience: '',
      activityName: '',
      clientExperienceShort: '',
      clientExperienceFull: '',
      activityDuration: '',
      activityPeopleCount: '',
      price: '',
      media: {
        mainImage: null,
        gallery: null
      }
    }
  },
  setCurrentActivityByTmpData (state, data) {
    state.activity = {
      tmpId: data.id,
      lastUpdatedAt: data.lastUpdatedAt,
      location: data.location,
      subject: data.subject,
      audience: data.audience,
      organizerExperience: data.organizerExperience,
      activityName: data.activityName,
      clientExperienceShort: data.clientExperienceShort,
      clientExperienceFull: data.clientExperienceFull,
      activityDuration: data.activityDuration,
      activityPeopleCount: data.activityPeopleCount,
      price: data.price,
      media: data.media
    }
  },
  setCurrentActivityByData (state, data) {
    state.activity = {
      tmpId: data.id,
      lastUpdatedAt: data.lastUpdatedAt,
      location: data.location,
      subject: data.subject,
      audience: data.audience,
      organizerExperience: data.organizerExperience,
      activityName: data.activityName,
      clientExperienceShort: data.clientExperienceShort,
      clientExperienceFull: data.clientExperienceFull,
      activityDuration: data.activityDuration,
      activityPeopleCount: data.activityPeopleCount,
      price: data.price,
      media: data.media
    }
  },
  setActivityDateDate (state, date) {
    state.activityDate.date = date
  },
  setActivityDateTime (state, time) {
    state.activityDate.time = time
  },
  setActivityDateStartPointDescription (state, desc) {
    state.activityDate.startPointDescription = desc
  },
  setActivityDates (state, dates) {
    state.activityDatesList = dates
  },
  clearActivityDate (state) {
    state.activityDate = {
      date: null,
      time: null,
      startPointDescription: '',
      lat: null,
      lon: null
    }
  },
  setActivityDateLat (state, lat) {
    state.activityDate.lat = lat
  },
  setActivityDateLon (state, lon) {
    state.activityDate.lon = lon
  }
}
