export default {
  async getActivitySubjects ({ commit, rootState }) {
    await this.$axios.$get('/api/activity/subjects')
      .then((res) => {
        commit('setActivitySubjects', res)
      })
  },
  async storeTmpActivity ({ commit, state }) {
    await this.$axios.post('/api/activity/storetmp', state.activity).then((res) => {
      commit('setActivityTmpId', res.data.success.tmpId)
      commit('setActivityLastUpdatedAt', res.data.success.updatedAt)
    })
  },
  storeActivity ({ commit, state }) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/activity', state.activity).then((response) => {
        commit('setCurrentActivityToNull')
        resolve(response)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  updateActivity ({ commit, state }) {
    return new Promise((resolve, reject) => {
      this.$axios.put('/api/activity/' + state.activity.tmpId, state.activity).then((response) => {
        commit('setCurrentActivityToNull')
        resolve(response)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  async preloadTmpActivity ({ commit, rootState }, id) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/activities/' + id + '?type=draft')
      .then((res) => {
        commit('setCurrentActivityByTmpData', res.data)
      })
  },
  async getActivityMedia ({ commit, rootState }, id) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/activities/tmpmedia/' + id)
      .then((res) => {
        commit('setCurrentActivityMedia', res)
      })
  },
  async getPostedActivityMedia ({ commit, rootState }, id) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/activities/media/' + id)
      .then((res) => {
        commit('setCurrentActivityMedia', res)
      })
  },
  async deleteActivityMediaItem ({ commit, rootState }, id) {
    await this.$axios.$delete('/api/user/' + rootState.auth.user.id + '/activities/tmpdeletemedia/' + id)
      .then((res) => {
        commit('setCurrentActivityMedia', res)
      })
  },
  async deletePostedActivityMediaItem ({ commit, rootState }, id) {
    await this.$axios.$delete('/api/user/' + rootState.auth.user.id + '/activities/deletemedia/' + id)
      .then((res) => {
        commit('setCurrentActivityMedia', res)
      })
  },
  async preloadActivity ({ commit, rootState }, id) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/activities/' + id + '?type=posted')
      .then((res) => {
        commit('setCurrentActivityByData', res.data)
      })
  },
  async createActivityDate ({ commit, state, dispatch }, activityId) {
    await this.$axios.post('/api/activity/' + activityId + '/dates', state.activityDate).then((res) => {
      dispatch('wait/start', 'loading activity dates', { root: true })
      dispatch('getActivityDates', activityId)
      commit('clearActivityDate')
    })
  },
  async getActivityDates ({ commit, state, dispatch }, activityId) {
    await this.$axios.$get('/api/activity/' + activityId + '/dates')
      .then((res) => {
        commit('setActivityDates', res.data)
        dispatch('wait/end', 'loading activity dates', { root: true })
      })
  }
}
