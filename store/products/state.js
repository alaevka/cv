export default () => ({
  productsList: [],
  filteredProductsList: null,
  filter: {
    searchByName: '',
    searchByPriceMin: '',
    searchByPriceMax: ''
  },
  foundedClassificators: [],
  product: {
    dictionaryId: null,
    id: null,
    name: null,
    description: '',
    price: '',
    costPrice: '',
    purchasePricePlan: '',
    purchasePriceFact: '',
    purchasePriceLast: '',
    isWeightedGoods: 0,
    type: null,
    currency: 'RUB',
    status: 'Active',
    weight: null,
    weightStep: 100,
    measure: 'г',
    vitamineId: null,
    lastUpdatedAt: null,
    media: {
      mainImage: null,
      gallery: null,
      video: null
    },
    originalMedia: [],
    attributes: [],
    brand: null,
    monitoringPrices: [],
    purchaseQuant: '',
    purchaseQuantMeasure: 'г',
    optPrice: '',
    packPrice1: '',
    packPrice2: '',
    otherPrice: ''
  },
  pagination: {
    links: null,
    meta: null
  },
  productAttributes: [],
  filteredAttributes: null,
  attributesFilter: {
    name: ''
  }
})
