import * as ProductsFilterHelperComponent from '~/utils/productsFilter'
import * as AttributesFilterHelperComponent from '~/utils/attributesFilter'

export default {
  setProductName (state, value) {
    state.product.name = value
  },
  setProductDescription (state, value) {
    state.product.description = value
  },
  setProductPrice (state, value) {
    state.product.price = value
  },
  setProductPurchasePricePlan (state, value) {
    state.product.purchasePricePlan = value
  },
  setProductPurchasePriceFact (state, value) {
    state.product.purchasePriceFact = value
  },
  setProductPurchasePriceLast (state, value) {
    state.product.purchasePriceLast = value
  },
  setProductPriceWithDiscount (state, value) {
    state.product.priceWithDiscount = value
  },
  setProductIsAction (state, value) {
    state.product.action = value
  },
  setProductIsNew (state, value) {
    state.product.new = value
  },
  setProductIsPopular (state, value) {
    state.product.popular = value
  },
  setProductIsHalyal (state, value) {
    state.product.halyal = value
  },
  setProductIsWeightedGoods (state, value) {
    state.product.isWeightedGoods = value
  },
  setProductCostPrice (state, value) {
    state.product.costPrice = value
  },
  setProductCurrency (state, value) {
    state.product.currency = value
  },
  setProductStatus (state, value) {
    state.product.status = value
  },
  setProductWeight (state, value) {
    state.product.weight = value
  },
  setProductWeightStep (state, value) {
    state.product.weightStep = value
  },
  setProductType (state, value) {
    state.product.type = value
  },
  setProductVitamine (state, value) {
    state.product.vitamineId = value
  },
  setProductTypeId (state, value) {
    state.product.type = value
  },
  setProductBrand (state, value) {
    state.product.brand = value
  },
  setProductMeasure (state, value) {
    state.product.measure = value
  },
  setProductId (state, val) {
    state.product.id = val
  },
  setProductLastUpdatedAt (state, val) {
    state.product.lastUpdatedAt = val
  },
  setCurrentProductMedia (state, data) {
    state.product.media = data
  },
  setImages (state, data) {
    state.product.media.gallery = data
  },
  setCurrentProductVideoMedia (state, data) {
    state.product.media.video = data
  },
  setProductDictionaryId (state, id) {
    state.product.dictionaryId = id
  },
  setProductAttributes (state, value) {
    state.productAttributes = value
    state.filteredAttributes = value
  },

  setProductPurchaseQuant (state, value) {
    state.product.purchaseQuant = value
  },
  setProductPurchaseQuantMeasure (state, value) {
    state.product.purchaseQuantMeasure = value
  },
  setProductOptPrice (state, value) {
    state.product.optPrice = value
  },
  setProductPackPrice1 (state, value) {
    state.product.packPrice1 = value
  },
  setProductPackPrice2 (state, value) {
    state.product.packPrice2 = value
  },
  setProductOtherPrice (state, value) {
    state.product.otherPrice = value
  },

  setCurrentProductToNull (state) {
    state.product = {
      dictionaryId: null,
      id: null,
      name: null,
      description: '',
      price: '',
      costPrice: '',
      purchasePricePlan: '',
      purchasePriceFact: '',
      purchasePriceLast: '',
      priceWithDiscount: '',
      isWeightedGoods: 0,
      action: 0,
      new: 0,
      popular: 0,
      halyal: 0,
      currency: 'RUB',
      status: 'Active',
      weight: null,
      weightStep: 100,
      type: null,
      measure: 'г',
      lastUpdatedAt: null,
      vitamineId: null,
      media: {
        mainImage: null,
        gallery: null,
        video: null
      },
      attributes: [],
      brand: null,
      originalMedia: [],
      monitoringPrices: [],
      purchaseQuant: '',
      purchaseQuantMeasure: 'г',
      optPrice: '',
      packPrice1: '',
      packPrice2: '',
      otherPrice: ''
    }
  },
  setCurrentProductByData (state, data) {
    state.product = {
      id: data.id,
      name: data.name,
      description: data.description,
      price: data.price,
      purchasePricePlan: data.purchasePricePlan,
      purchasePriceFact: data.purchasePriceFact,
      purchasePriceLast: data.purchasePriceLast,
      priceWithDiscount: data.priceWithDiscount,
      isWeightedGoods: data.isWeightedGoods,
      costPrice: data.costPrice,
      currency: data.currency,
      status: data.status,
      weight: data.weight,
      weightStep: data.weightStep,
      type: data.category,
      measure: data.measure,
      lastUpdatedAt: data.lastUpdatedAt,
      media: data.media,
      attributes: data.attributes,
      brand: data.brand,
      monitoringPrices: data.monitoringPrices,
      vitamineId: data.vitamineId,
      action: data.action,
      new: data.new,
      popular: data.popular,
      halyal: data.halyal,
      url: data.url,
      purchaseQuant: data.purchaseQuant,
      purchaseQuantMeasure: data.purchaseQuantMeasure,
      optPrice: data.optPrice,
      packPrice1: data.packPrice1,
      packPrice2: data.packPrice2,
      otherPrice: data.otherPrice
    }
  },
  setProducts (state, products) {
    state.productsList = products
    state.filteredProductsList = products
  },
  setSearchByNameProductFilter (state, value) {
    state.filter.searchByName = value
  },
  setSearchByPriceMinProductFilter (state, value) {
    state.filter.searchByPriceMin = value
  },
  setSearchByPriceMaxProductFilter (state, value) {
    state.filter.searchByPriceMax = value
  },
  filterProducts (state) {
    const products = [...state.productsList]
    state.filteredProductsList = products
    state.filteredProductsList = ProductsFilterHelperComponent.filterProducts(state.filter, products)
  },
  clearFilters (state) {
    const products = [...state.productsList]
    state.filter = {
      searchByName: '',
      searchByPriceMin: '',
      searchByPriceMax: ''
    }
    state.filteredProductsList = ProductsFilterHelperComponent.filterProducts(state.filter, products)
  },
  setFoundedClassificators (state, data) {
    state.foundedClassificators = data
  },
  setPaginationLinks (state, links) {
    state.pagination.links = links
  },
  setPaginationMeta (state, meta) {
    state.pagination.meta = meta
  },
  setSearchByAttributeName (state, value) {
    state.attributesFilter.name = value
  },
  filterAttributes (state) {
    const productAttributes = [...state.productAttributes]
    state.filteredAttributes = productAttributes
    state.filteredAttributes = AttributesFilterHelperComponent.filterAttributes(state.attributesFilter, productAttributes)
  },
  addAttribute (state, attribute) {
    state.product.attributes.push({
      id: attribute.id,
      name: attribute.name,
      value: ''
    })
  },
  setAttribute (state, data) {
    state.product.attributes[data.index].value = data.value
  },
  setCopiedProductAttributes (state, attributes) {
    state.product.attributes = []
    attributes.forEach(function (attribute) {
      state.product.attributes.push({
        id: attribute.attrId,
        name: attribute.attrName,
        value: attribute.attrVal
      })
    })
  },
  deleteProductByIndex (state, index) {
    state.filteredProductsList.splice(index, 1)
  },
  deleteProductAttributeByIndex (state, index) {
    state.product.attributes.splice(index, 1)
  },
  setCurrentProductOriginalMedia (state, data) {
    state.product.originalMedia = data
  },
  setProductMonitoringPrices (state, data) {
    state.product.monitoringPrices = data
  }

}
