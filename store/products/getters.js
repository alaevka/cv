export default {
  allProductsList: state => state.productsList,

  // getCourse: (state) => state.course,

  // getSearchWord: (state) => state.searchWord,

  getFilteredProductList: state => state.filteredProductsList,

  getFilteredAttributes: state => state.filteredAttributes
}
