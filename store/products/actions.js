export default {
  async storeDraftProduct ({ commit, state }) {
    await this.$axios.post('/api/product/storetmp', state.product).then((res) => {
      commit('setProductId', res.data.success.id)
      commit('setProductLastUpdatedAt', res.data.success.updatedAt)
    })
  },
  async getProductMedia ({ commit, state }, id) {
    await this.$axios.$get('/api/product/media/' + id)
      .then((res) => {
        commit('setCurrentProductMedia', res)
      })
  },
  async getProductVideoMedia ({ commit, state }, id) {
    await this.$axios.$get('/api/product/videomedia/' + id)
      .then((res) => {
        commit('setCurrentProductVideoMedia', res)
      })
  },
  async loadingProductsAttributes ({ commit }) {
    await this.$axios.$get('/api/product/attributes')
      .then((res) => {
        commit('setProductAttributes', res.data)
      })
  },
  async deleteProductMediaItem ({ commit, rootState }, id) {
    await this.$axios.$delete('/api/product/deletemedia/' + id)
      .then((res) => {
        commit('setCurrentProductMedia', res)
      })
  },
  storeProduct ({ commit, state }) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/product', state.product).then((response) => {
        commit('setCurrentProductToNull')
        resolve(response)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  async preloadProduct ({ commit, rootState }, id) {
    await this.$axios.$get('/api/product/' + id + '?type=active')
      .then((res) => {
        commit('setCurrentProductByData', res.data)
      })
  },
  updateProduct ({ commit, state }) {
    return new Promise((resolve, reject) => {
      this.$axios.put('/api/product/' + state.product.id, state.product).then((response) => {
        commit('setCurrentProductToNull')
        resolve(response)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  async deleteProduct ({ commit, state, dispatch }, data) {
    await this.$axios.$delete('/api/product/' + data.id)
      .then((res) => {
        // commit('setProducts', res.data)
        commit('deleteProductByIndex', data.index)
        if (state.filteredProductsList.length === 0) {
          dispatch('loadProductsByPageForProfile', { page: 1, type: data.type, user: 'currentUser' })
          this.$router.push({
            query: {
              // ...this.$route.query,
              type: data.type,
              page: 1

            }
          })
        }
        // dispatch('getProducts', data.type)
      })
  },
  async getProducts ({ commit, state }, type) {
    await this.$axios.$get('/api/product?type=' + type, { filter: state.filter })
      .then((res) => {
        commit('setProducts', res.data)
        commit('setPaginationLinks', res.links)
        commit('setPaginationMeta', res.meta)
        // dispatch('filterProducts')
      })
  },
  // async loadProductsByPage ({ commit, state, dispatch }, page) {
  //   dispatch('wait/start', 'loading user products', { root: true })
  //   await this.$axios.$post('/api/product/search?page=' + page, { filter: state.filter })
  //     .then((res) => {
  //       commit('setProducts', res.data)
  //       commit('setPaginationLinks', res.links)
  //       commit('setPaginationMeta', res.meta)
  //       dispatch('wait/end', 'loading user products', { root: true })
  //     })
  // },
  async loadProductsByPageForProfile ({ commit, state, dispatch }, data) {
    dispatch('wait/start', 'loading user products', { root: true })
    await this.$axios.$post('/api/product/search?type=' + data.type + '&page=' + data.page + '&user=' + data.user, { filter: state.filter })
      .then((res) => {
        commit('setProducts', res.data)
        commit('setPaginationLinks', res.links)
        commit('setPaginationMeta', res.meta)
        dispatch('wait/end', 'loading user products', { root: true })
      })
  },
  async restoreProduct ({ commit, rootState, dispatch }, id) {
    await this.$axios.$put('/api/product/restore/' + id)
      .then((res) => {
        dispatch('filterProducts')
      })
  },
  async filterSearchByName ({ commit, dispatch }, search) {
    await commit('setSearchByNameProductFilter', search)
    dispatch('filterProducts')
  },
  async filterSearchByPriceMin ({ commit, dispatch }, search) {
    await commit('setSearchByPriceMinProductFilter', search)
    dispatch('filterProducts')
  },
  async filterSearchByPriceMax ({ commit, dispatch }, search) {
    await commit('setSearchByPriceMaxProductFilter', search)
    dispatch('filterProducts')
  },
  // async filterProducts ({ commit }) {
  //   await commit('filterProducts')
  // },
  async filterProducts ({ commit, state, dispatch }) {
    dispatch('wait/start', 'loading user products', { root: true })
    await this.$axios.$post('/api/product/search?type=active', { filter: state.filter })
      .then((res) => {
        commit('setProducts', res.data)
        commit('setPaginationLinks', res.links)
        commit('setPaginationMeta', res.meta)
        dispatch('wait/end', 'loading user products', { root: true })
      })
  },
  findClassifications ({ commit, state }, search) {
    return new Promise((resolve, reject) => {
      this.$axios.get('/api/product/searchokp/' + search).then((response) => {
        // commit('setFoundedClassificators', response)
        resolve(response)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  findBrand ({ commit, state }, search) {
    return new Promise((resolve, reject) => {
      this.$axios.get('/api/product/searchbrand/' + search).then((response) => {
        // commit('setFoundedClassificators', response)
        resolve(response)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  async resetFilters ({ commit, dispatch }, shopId) {
    await commit('clearFilters')
    dispatch('filterProducts')
  },
  // async searchAvailableProducts ({ commit, dispatch }, search) {
  //   await commit('setSearchByNameProductFilter', search)
  //   dispatch('filterProducts')
  // }
  async searchByAttributeName ({ commit, dispatch }, search) {
    await commit('setSearchByAttributeName', search)
    commit('filterAttributes')
  },
  async getProductMonitoringPrices ({ commit, state }) {
    await this.$axios.$get('/api/product/monitoringprices/' + state.product.id)
      .then((res) => {
        commit('setProductMonitoringPrices', res)
      })
  },
  updateImagesOrder ({ commit }, value) {
    commit('setImages', value)
    this.$axios.$post('/api/product/updatemedia', { gallery: value })
      .then((res) => {

      })
  }
}
