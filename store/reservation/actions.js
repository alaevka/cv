export default {
  store ({ commit, rootState }) {
    return new Promise((resolve, reject) => {
      const data = {
        reservationParams: rootState.reservation,
        filterParams: rootState.filter
      }
      this.$axios.$post('/api/reservation', data)
        .then((res) => {
          commit('setReservationNumber', res.success.booking.id)
          commit('setLastBooking', res.success.booking)
          commit('setObjectSelected', 'not_selected')
          commit('setRoomSelected', 'not_selected')
          resolve(res)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  storeDate ({ commit, rootState }) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/reservation/activity', { date: rootState.reservation.reservationDate, userId: rootState.auth.user.id }).then((response) => {
        // commit('setCurrentActivityToNull')
        resolve(response)
      }).catch((err) => {
        reject(err)
      })
    })
  }
}
