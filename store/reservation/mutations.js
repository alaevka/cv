export default {
  setObjectSelected (state, hotel) {
    state.objectSelected = hotel
  },
  setRoomSelected (state, room) {
    state.roomSelected = room
  },
  setUserFirstName (state, userFirstName) {
    state.userReserve.userFirstName = userFirstName
  },
  setUserLastName (state, userLastName) {
    state.userReserve.userLastName = userLastName
  },
  setUserSurName (state, userSurName) {
    state.userReserve.userSurName = userSurName
  },
  setTravelType (state, travelType) {
    state.userReserve.travelType = travelType
  },
  setUserEmail (state, userEmail) {
    state.userReserve.userEmail = userEmail
  },
  setUserPhonePrefix (state, userPhonePrefix) {
    state.userReserve.userPhonePrefix = userPhonePrefix
  },
  setUserPhoneNumber (state, userPhoneNumber) {
    state.userReserve.userPhoneNumber = userPhoneNumber
  },
  setIsNeedTransfer (state, isNeedTransfer) {
    state.userReserve.isNeedTransfer = isNeedTransfer
  },
  setIsNeedACar (state, isNeedACar) {
    state.userReserve.isNeedACar = isNeedACar
  },
  setUserComment (state, userComment) {
    state.userReserve.userComment = userComment
  },
  setAuthUserId (state, id) {
    state.userReserve.authId = id
  },
  setReservationNumber (state, number) {
    state.reservationNumber = number
  },
  setLastBooking (state, booking) {
    state.lastBooking = booking
  },
  setReservationDate (state, date) {
    state.reservationDate = date
  }
}
