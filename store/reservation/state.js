export default () => ({
  userReserve: {
    authId: null,
    userFirstName: '',
    userLastName: '',
    userSurName: '',
    userEmail: '',
    userPhonePrefix: '+7',
    userPhoneNumber: '',
    travelType: '',
    isNeedTransfer: false,
    isNeedACar: false,
    userComment: ''
  },
  objectSelected: 'not_selected',
  roomSelected: 'not_selected',
  reservationNumber: null,
  lastBooking: null,
  reservationDate: 'not_selected'
})
