export default {

  firstBlockCategories (state) {
    const resultedArray = []
    state.popularClassificators.forEach(function (entry) {
      if (entry.id === 996 || entry.id === 997 || entry.id === 994 || entry.id === 998) {
        resultedArray.push(entry)
      }
    })
    return resultedArray
  },

  sortedByShopProducts (state) {
    // return state.items.filter(product => product.product.shopId === 5)
    // return ProductsCartFilterHelperComponent.filterProductsByShop('asc', state.items)
    const sortedByShopProducts = [...state.filteredProducts].sort(function (a, b) {
      return ((a.shopId === b.shopId) ? 0 : ((a.shopId > b.shopId) ? 1 : -1))
    })

    const resultedArray = []

    sortedByShopProducts.forEach(function (entry) {
      if (resultedArray.filter(e => e.shop === entry.shopId).length > 0) {
        const shopIndex = resultedArray.findIndex(shop => shop.shop === entry.shopId)
        resultedArray[shopIndex].products.push(entry)
      } else {
        resultedArray.push({ shop: entry.shopId, shopName: entry.shopName, shopPriceMin: entry.shopPriceMin, shopImage: entry.shopImage, products: [entry] })
      }
    })

    return resultedArray
    // return state.filteredProducts
  }
}
