export default {
  async getShops ({ commit, state }) {
    await this.$axios.$get('/api/shops')
      .then((res) => {
        commit('setShops', res.data)
      })
  },
  // async getShop ({ commit, state }, objectId) {
  //   await this.$axios.$get('/api/shops/' + objectId)
  //     .then((res) => {
  //       commit('setShop', res.data)
  //     })
  // },
  getShop ({ commit }, shopId) {
    return new Promise((resolve, reject) => {
      this.$axios.get(`api/shops/${shopId}`)
        .then((res) => {
          commit('setShop', res.data.data)
          resolve(res)
        }).catch((err) => {
          reject(err)
        })
    })
  },

  async getCarouselProducts ({ commit, state }) {
    await this.$axios.$get('/api/shops/carousel')
      .then((res) => {
        commit('setCarouselProducts', res.data)
      })
  },
  async getUpdateCarouselProducts ({ commit, state }) {
    await this.$axios.$get('/api/shops/checkproductsjustbuyed')
      .then((res) => {
        commit('setUpdateCarouselProducts', res.data)
      })
  },
  async getMainCategories ({ commit, state }) {
    await this.$axios.$get('/api/shops/categories')
      .then((res) => {
        commit('setMainCategories', res)
      })
  },
  getSubCategories ({ state, dispatch }, categoryId) {
    // return new Promise((resolve, reject) => {
    if (state.subCategories.length > 0) {
      const result = state.subCategories.filter(x => x.mainCategoryId === categoryId)

      if (result.length > 0) {
        return result[0].subcategories
      } else {
        return dispatch('loadSubCategories', categoryId)
      }
    } else {
      return dispatch('loadSubCategories', categoryId)
    }
  },
  loadSubCategories ({ commit, state }, id) {
    return new Promise((resolve, reject) => {
      this.$axios.$get('/api/shops/categories/childs/' + id)
        .then((res) => {
          resolve(res.list)
          commit('pushSubCategories', { res, id })
        }).catch((err) => {
          reject(err)
        })
    })
  },
  async filterProducts ({ commit, state, dispatch }) {
    dispatch('wait/start', 'loading filtered products list', { root: true })
    await this.$axios.$post('/api/product/search', { filter: state.filter, sort: state.sort })
      .then((res) => {
        commit('filterProducts', res.data)
        commit('setPaginationLinks', res.links)
        commit('setPaginationMeta', res.meta)
        dispatch('wait/end', 'loading filtered products list', { root: true })
      })
  },
  async loadProductsByPage ({ commit, state, dispatch }, page) {
    dispatch('wait/start', 'loading filtered products list', { root: true })
    await this.$axios.$post('/api/product/search?page=' + page, state.filter)
      .then((res) => {
        commit('filterProducts', res.data)
        commit('setPaginationLinks', res.links)
        commit('setPaginationMeta', res.meta)
        dispatch('wait/end', 'loading filtered products list', { root: true })
      })
  },
  async getProductsSearchPopularClassificators ({ commit, state }, shopId) {
    if (shopId !== null) {
      await this.$axios.$get(`/api/shops/popularclassificators/${shopId}`)
        .then((res) => {
          commit('setPopularClassificators', res.data)
        })
    } else {
      await this.$axios.$get(`/api/shops/popularclassificators`)
        .then((res) => {
          commit('setPopularClassificators', res.data)
        })
    }
  },
  async filterSearchByName ({ commit, dispatch }, search) {
    await commit('setSearchByNameProductFilter', search)
    dispatch('filterProducts')
  },
  async filterSearchByPriceMin ({ commit, dispatch }, search) {
    await commit('setSearchByPriceMinProductFilter', search)
    dispatch('filterProducts')
  },
  async sortBy ({ commit, dispatch }, field) {
    await commit('setSortField', field)
    dispatch('filterProducts')
  },
  async filterSearchByPriceMax ({ commit, dispatch }, search) {
    await commit('setSearchByPriceMaxProductFilter', search)
    dispatch('filterProducts')
  },
  async filterSearchByShop ({ commit, dispatch }, search) {
    await commit('setSearchByShopProductFilter', search)
    dispatch('filterProducts')
  },
  async resetFilters ({ commit, dispatch }, shopId) {
    if (shopId !== null) {
      await commit('clearFiltersWithoutShop', shopId)
    } else {
      await commit('clearFilters')
    }
    dispatch('filterProducts')
  },
  async filterSearchByClassificator ({ commit, dispatch }, id) {
    await commit('setSearchByClassificatorFilter', id)
    dispatch('filterProducts')
  },
  getProduct ({ commit }, params) {
    return new Promise((resolve, reject) => {
      this.$axios.get(`api/product/info/${params.pathMatch}`)
        .then((res) => {
          commit('setProduct', res.data.data)
          resolve(res)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  async getProductsForMainPage ({ commit, dispatch }) {
    dispatch('wait/start', 'loading filtered products list', { root: true })
    await this.$axios.$get('/api/product/market')
      .then((res) => {
        commit('filterProducts', res.data)
        dispatch('wait/end', 'loading filtered products list', { root: true })
      })
  },
  async globalSearch ({ commit }, query) {
    await this.$axios.$post('/api/product/globalsearch', { query })
      .then((res) => {
        commit('setGlobalSearchResults', res.results)
      })
  },
  async getMainPageData ({ commit, dispatch }) {
    await this.$axios.$get('/api/index')
      .then((res) => {
        commit('setIndexData', res.data)
      })
  },
  async getProducts ({ commit, state, dispatch }, pageNum) {
    dispatch('wait/start', 'loading shop products list', { root: true })
    await this.$axios.$post('/api/product/byshop?page=' + pageNum, { filter: state.filter, sort: state.sort })
      .then((res) => {
        commit('filterProducts', res.data)
        commit('setPaginationLinks', res.links)
        commit('setPaginationMeta', res.meta)
        dispatch('wait/end', 'loading shop products list', { root: true })
      })
  },
  storeSearchQuery ({ state }, query) {
    // console.log(query)
    // const userSearchHistory = this.$cookies.get('search_history')
    // if (userSearchHistory) {
    //   const arr = JSON.parse(userSearchHistory)
    //   arr.push(query)
    //   await this.$cookies.set('search_history', JSON.stringify(arr))
    // } else {
    //   const arr = []
    //   arr.push(query)
    //   await this.$cookies.set('search_history', JSON.stringify(arr))
    // }
  }
}
