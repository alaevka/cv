export default () => ({
  shopsList: [],
  currentShop: [],
  carouselProductsList: [],
  filteredProducts: [],
  filter: {
    searchByName: '',
    searchByPriceMin: '',
    searchByPriceMax: '',
    classificators: '',
    searchByShop: null
  },
  sort: {
    field: 'id',
    direction: 'asc'
  },
  pagination: {
    links: null,
    meta: {
      total: null,
      last_page: 0
    }
  },
  viewBy: 'grid',
  popularClassificators: [],
  mainCategories: [],
  subCategories: [],
  product: null,
  globalSearchResults: null,
  indexData: null,
  mainPageShopsData: null
})
