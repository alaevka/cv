export default {
  setShops (state, data) {
    state.shopsList = data
  },
  setShop (state, data) {
    state.currentShop = data
  },
  setCarouselProducts (state, data) {
    state.carouselProductsList = data
  },
  setUpdateCarouselProducts (state, data) {
    // data.concat(state.carouselProductsList)
    state.carouselProductsList.splice(-1, data.length)
    state.carouselProductsList.unshift(...data)
  },
  filterProducts (state, data) {
    state.filteredProducts = data
    // const products = [...state.productsList]
    // state.filteredProductsList = products
    // state.filteredProductsList = ProductsFilterHelperComponent.filterProducts(state.filter, products)
  },
  setPaginationLinks (state, links) {
    state.pagination.links = links
  },
  setPaginationMeta (state, meta) {
    state.pagination.meta = meta
  },
  setPopularClassificators (state, data) {
    state.popularClassificators = data
  },
  setSearchByNameProductFilter (state, value) {
    state.filter.searchByName = value
  },
  setSearchByPriceMinProductFilter (state, value) {
    state.filter.searchByPriceMin = value
  },
  setSearchByPriceMaxProductFilter (state, value) {
    state.filter.searchByPriceMax = value
  },
  setSearchByShopProductFilter (state, value) {
    state.filter.searchByShop = value
  },
  setSearchByClassificatorFilter (state, value) {
    state.filter.classificators = value
  },
  clearFilters (state) {
    state.filter = {
      searchByName: '',
      searchByPriceMin: '',
      searchByPriceMax: '',
      classificators: '',
      searchByShop: null
    }
  },
  clearFiltersWithoutShop (state, shopId) {
    state.filter = {
      searchByName: '',
      searchByPriceMin: '',
      searchByPriceMax: '',
      classificators: '',
      searchByShop: shopId
    }
  },
  setMainCategories (state, data) {
    state.mainCategories = data
  },
  pushSubCategories (state, data) {
    // console.log(data)
    state.subCategories.push({ mainCategoryId: data.id, subcategories: data.res.list })
  },
  setViewBy (state, val) {
    state.viewBy = val
  },
  setProduct (state, data) {
    state.product = data
  },
  setSortField (state, field) {
    state.sort.field = field
    if (state.sort.direction === 'asc') {
      state.sort.direction = 'desc'
    } else {
      state.sort.direction = 'asc'
    }
  },
  setGlobalSearchResults (state, data) {
    state.globalSearchResults = data
  },
  setIndexData (state, data) {
    state.indexData = data
  },
  setMainPageShopsData (state, data) {
    state.mainPageShopsData = data
  },
  setShopIdFilter (state, shopId) {
    state.filter.searchByShop = shopId
  },
  resetGlobalSearchResults (state) {
    state.globalSearchResults = null
  }
}
