import { checkbox } from '@tailwindcss/custom-forms/src/defaultOptions'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const state = () => ({
  justRegistered: false,
  counter: 60,
  domain: null,
  user: {
    phone: ''
  },
  currentCity: 'Москва'
})

export const actions = {

  async nuxtServerInit ({ commit }, reqObject) {
    // const userCheckoutID = reqObject.$cookies.get('rfmCheckoutId')
    let userCheckoutID = null
    if (reqObject.$auth.loggedIn) {
      userCheckoutID = reqObject.$cookies.get('rfmCheckoutId_' + reqObject.$auth.user.id)
    } else {
      userCheckoutID = reqObject.$cookies.get('rfmCheckoutId_')
    }

    if (userCheckoutID) {
      await this.$axios.$get('/api/orders/cart/' + userCheckoutID)
        .then((res) => {
          if (!res.completedAt) {
            commit('cart/setItems', JSON.parse(res.items))
          }
        })
    }

    commit('setDomain', reqObject.req.headers.host)
  },

  async updateProfile ({ commit, rootState }) {
    const profile = rootState.auth.user.profile
    await this.$axios.$put('/api/user/' + rootState.auth.user.id + '/profile/' + profile.id, profile)
      .then((res) => {
        commit('updateUserRole', res.role)
      })
  },
  login ({ commit }, phone) {
    return new Promise((resolve, reject) => {
      this.$axios.$post('/api/login', { phone })
        .then((response) => {
          const phoneFormated = phone.phone
          commit('setPhone', {
            phoneFormated
          })
          commit('renewCounter')
          resolve(response)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  register ({ commit }, user) {
    return new Promise((resolve, reject) => {
      const data = user.user

      this.$axios.$post('/api/register', {
        firstName: data.firstName,
        lastName: data.lastName,
        surName: data.surName,
        email: data.email,
        phone: data.phone.prefix + data.phone.number
      })
        .then((response) => {
          const phoneFormated = data.phone.prefix + data.phone.number
          commit('setPhone', {
            phoneFormated
          })
          commit('setjustRegistered', true)
          resolve(response)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  refresh ({ commit, state }) {
    return new Promise((resolve, reject) => {
      const phone = state.user.phone
      this.$axios.$post('/api/refresh', { phone })
        .then((response) => {
          resolve(response)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  verify ({ commit, state }, confirmationCode) {
    return new Promise((resolve, reject) => {
      const data = confirmationCode
      const phone = state.user.phone

      this.$auth.loginWith('local', {
        data: {
          sms: data.confirmationCode,
          phone
        }
      }).then((response) => {
        // replace user rfmCheckoutId_
        const checkoutId = this.$cookies.get('rfmCheckoutId_')
        if (checkoutId) {
          if (this.$auth.loggedIn) {
            this.$cookies.set('rfmCheckoutId_' + this.$auth.user.id, checkoutId)
            this.$cookies.remove('rfmCheckoutId_')
          }
        }

        resolve(response)
      }).catch((e) => {
        reject(e)
      })

      // this.$axios.$post('http://127.0.0.1:8000/api/verify', {
      //   code: data.confirmationCode,
      //   phone
      // })
      // .then((response) => {
      //   resolve(response)
      // })
      // .catch((err) => {
      //   reject(err)
      // })
    })
  },
  logout ({ commit }) {
    return new Promise((resolve, reject) => {
      this.$auth.logout('local').then((response) => {
        resolve(response)
      })
    })
  }
}

export const mutations = {
  decrement (state) {
    if (state.counter > 0) {
      state.counter--
    }
  },
  setPhone (state, payload) {
    state.user.phone = payload.phoneFormated
  },
  setjustRegistered (state, status) {
    state.justRegistered = status
  },
  renewCounter (state) {
    state.counter = 60
  },
  updateProfileType (state, type) {
    state.auth.user.profile.profile_type = type
  },
  updateSearchPrivelege (state, privelege) {
    state.auth.user.profile.searchPrivelege = privelege
  },
  updateVkProfile (state, profile) {
    state.auth.user.profile.social_id_vk = profile
  },
  updateFbProfile (state, profile) {
    state.auth.user.profile.social_id_facebook = profile
  },
  updateInstProfile (state, profile) {
    state.auth.user.profile.social_id_instagram = profile
  },
  updateLocationProfileField (state, location) {
    state.auth.user.profile.location = location
  },
  updateAddrProfileField (state, address) {
    state.auth.user.profile.address = address
  },
  updateShopNameProfileField (state, name) {
    state.auth.user.profile.shop_name = name
  },
  updateUserRole (state, data) {
    state.auth.user.role = data
  },
  updateCarNameProfileField (state, name) {
    state.auth.user.profile.car_name = name
  },
  updateCarNumberProfileField (state, number) {
    state.auth.user.profile.car_number = number
  },
  setDomain (state, domain) {
    state.domain = domain
  },
  updateCurrentCity (state, city) {
    state.currentCity = city
  }
}

export const getters = {
  counter (state) {
    return state.counter
  },
  phone (state) {
    return state.user.phone
  }
}
