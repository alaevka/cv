// const cart = window.localStorage.getItem('cart')
export default () => ({
  items: [], // cart ? JSON.parse(cart) : [],
  user: {
    id: null,
    firstName: '',
    lastName: '',
    surName: '',
    phone: '',
    email: '',
    location: null,
    address: {},
    frontDoor: '',
    apartment: '',
    payment: 'online',
    deliveryTime: ['10:00', '19:00'],
    comment: '',
    deliveryDate: ''
  },
  promocode: null
})
