export default {
  repeatProductsInCart (state, products) {
    products.map(function (product) {
      state.items.push({ product: product.product, quantity: product.quantity, minWeight: product.product.weight, weightStep: product.product.weightStep, weight: product.weight, isWeighted: product.product.isWeightedGoods, shopId: product.product.shopId, shopName: product.product.shopName, shopPriceMin: product.product.shopPriceMin })
    })
  },

  pushProductToCart (state, product) {
    if (state.items.filter(e => e.product.id === product.id).length > 0) {
      const CartProduct = state.items.find(prod => prod.product.id === product.id)
      // console.log(CartProduct)
      if (parseInt(CartProduct.isWeighted) === 1) {
        CartProduct.weight = CartProduct.weight + CartProduct.minWeight
      } else {
        CartProduct.quantity++
      }
    } else {
      state.items.push({ product, quantity: 1, minWeight: product.weight, weightStep: product.weightStep, weight: product.weight, isWeighted: product.isWeightedGoods, shopId: product.shopId, shopName: product.shopName, shopPriceMin: product.shopPriceMin })
    }
  },
  saveCart (state) {
    // if (process.client) {
    //   window.localStorage.setItem('cart', JSON.stringify(state.items))
    // }
  },
  removeFromCart: (state, productId) => {
    // find the product in the products list
    // const CartProduct = state.items.find(product => product.product.id === productId)
    // CartProduct.quantity--
    const cartProductIndex = state.items.findIndex(product => product.product.id === productId)
    state.items.splice(cartProductIndex, 1)
  },
  removeShopFromCart: (state, shopId) => {
    // const indexes = state.items.reduce(function (a, e, i) {
    //   if (e.shopId === shopId) { a.push(i) }
    //   return a
    // }, [])
    // indexes.forEach(function (index) {
    //   state.items.splice(index, 1)
    // })
    state.items = state.items.filter(function (obj) {
      return obj.shopId !== shopId
    })
  },
  plusOneItemOfProductInCart: (state, productId) => {
    const CartProduct = state.items.find(product => product.product.id === productId)
    if (parseInt(CartProduct.isWeighted) === 1) {
      CartProduct.weight = CartProduct.weight + CartProduct.weightStep
    } else {
      CartProduct.quantity++
    }
  },
  minusOneItemOfProductInCart: (state, productId) => {
    const CartProduct = state.items.find(product => product.product.id === productId)
    if (parseInt(CartProduct.isWeighted) === 1) {
      const newWeight = CartProduct.weight - CartProduct.weightStep
      if (newWeight < CartProduct.minWeight) {
        const cartProductIndex = state.items.findIndex(product => product.product.id === productId)
        state.items.splice(cartProductIndex, 1)
      } else {
        CartProduct.weight = CartProduct.weight - CartProduct.weightStep
      }
    } else {
      CartProduct.quantity--

      if (CartProduct.quantity === 0) {
        const cartProductIndex = state.items.findIndex(product => product.product.id === productId)
        state.items.splice(cartProductIndex, 1)
      }
    }
  },
  setUserFirstName (state, value) {
    state.user.firstName = value
  },
  setUserLastName (state, value) {
    state.user.lastName = value
  },
  setUserSurName (state, value) {
    state.user.surName = value
  },
  setUserPhone (state, value) {
    state.user.phone = value
  },
  setUserEmail (state, value) {
    state.user.email = value
  },
  setUserLocation (state, data) {
    state.user.location = data
  },
  setUserAddress (state, value) {
    state.user.address = value
  },
  setUserFrontDoor (state, value) {
    state.user.frontDoor = value
  },
  setUserApartment (state, value) {
    state.user.apartment = value
  },
  setUserPayment (state, value) {
    state.user.payment = value
  },
  setDeliveryTime (state, value) {
    state.user.deliveryTime = value
  },
  setUserComment (state, value) {
    state.user.comment = value
  },
  setDeliveryDate (state, data) {
    state.user.deliveryDate = data
  },
  setCurrentUserInfoFromProfile: (state, infoFromProfile) => {
    state.user.id = infoFromProfile.id
    state.user.firstName = infoFromProfile.firstName
    state.user.lastName = infoFromProfile.lastName
    state.user.surName = infoFromProfile.surName
    state.user.phone = infoFromProfile.phone
    state.user.email = infoFromProfile.email
    state.user.location = infoFromProfile.profile.location
    state.user.address = infoFromProfile.profile.address
  },
  setEmptyCurrentUserInfo: (state) => {
    state.user.id = ''
    state.user.firstName = ''
    state.user.lastName = ''
    state.user.surName = ''
    state.user.phone = ''
    state.user.email = ''
    state.user.location = null
    state.user.address = null
  },
  setCartToNull (state, value) {
    state.items = []
    state.promocode = null
  },
  setPromocode (state, value) {
    state.promocode = value
  },
  deletePromocode (state) {
    state.promocode = null
  },
  setItems (state, items) {
    state.items = items
  }
}
