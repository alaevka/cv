import * as moment from 'moment'

export default {
  saveCart ({ rootState }) {
    let userCheckoutID = null
    let userId = null
    if (this.$auth.loggedIn) {
      userCheckoutID = this.$cookies.get('rfmCheckoutId_' + rootState.auth.user.id)
      userId = rootState.auth.user.id
    } else {
      userCheckoutID = this.$cookies.get('rfmCheckoutId_')
    }

    this.$axios.$post('/api/orders/cart', { items: rootState.cart.items, promocode: rootState.cart.promocode, uuid: userCheckoutID || null, userId })
      .then((res) => {
        if (!userCheckoutID) {
          if (this.$auth.loggedIn) {
            this.$cookies.set('rfmCheckoutId_' + rootState.auth.user.id, res.uuid)
          } else {
            this.$cookies.set('rfmCheckoutId_', res.uuid)
          }
        }
      })
  },
  addProductToCart ({ dispatch, commit }, product) {
    commit('pushProductToCart', product)
    dispatch('saveCart')
  },
  removeFromCart: ({ dispatch, commit }, productId) => {
    commit('removeFromCart', productId)
    dispatch('saveCart')
  },
  removeShopFromCart: ({ dispatch, commit }, shopId) => {
    commit('removeShopFromCart', shopId)
    dispatch('saveCart')
  },
  plusOneItemOfProductInCart: ({ dispatch, commit }, productId) => {
    commit('plusOneItemOfProductInCart', productId)
    dispatch('saveCart')
  },
  minusOneItemOfProductInCart: ({ dispatch, commit }, productId) => {
    commit('minusOneItemOfProductInCart', productId)
    dispatch('saveCart')
  },
  getUserInfo ({ commit, rootState }) {
    if (this.$auth.loggedIn) {
      commit('setCurrentUserInfoFromProfile', rootState.auth.user)
    } else {
      commit('setEmptyCurrentUserInfo')
    }
  },
  checkout ({ dispatch, commit, state, getters }) {
    return new Promise((resolve, reject) => {
      const deldate = moment(state.user.deliveryDate).format('YYYY-MM-DD')

      const data = { deliveryDate: deldate, user: state.user, items: getters.sortedByShopProducts, promocode: state.promocode, checkoutId: this.$auth.loggedIn ? this.$cookies.get('rfmCheckoutId_' + this.$auth.user.id) : this.$cookies.get('rfmCheckoutId_') }

      this.$axios.post('/api/orders', data).then((response) => {
        if (this.$auth.loggedIn) {
          this.$cookies.remove('rfmCheckoutId_' + this.$auth.user.id)
        }
        this.$cookies.remove('rfmCheckoutId_')
        commit('setCartToNull')
        resolve(response)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  checkPromocode ({ commit, state }, promocode) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/orders/checkpromocode', { code: promocode }).then((response) => {
        resolve(response)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  repeatOrder ({ dispatch, commit }, id) {
    this.$axios.get('/api/orders/repeat/' + id).then((response) => {
      commit('setCartToNull')
      commit('repeatProductsInCart', response.data)
      dispatch('saveCart')
    })
  },
  setPromocode: ({ commit }, promocode) => {
    commit('setPromocode', promocode)
  },
  deletePromocode: ({ commit }) => {
    commit('deletePromocode')
  }
}
