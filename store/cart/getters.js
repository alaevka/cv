export default {
  sortedByShopProducts (state) {
    // return state.items.filter(product => product.product.shopId === 5)
    // return ProductsCartFilterHelperComponent.filterProductsByShop('asc', state.items)
    const sortedByShopProducts = [...state.items].sort(function (a, b) {
      return ((a.product.shopId === b.product.shopId) ? 0 : ((a.product.shopId > b.product.shopId) ? 1 : -1))
    })

    const resultedCartArray = []

    sortedByShopProducts.forEach(function (entry) {
      if (resultedCartArray.filter(e => e.shop === entry.shopId).length > 0) {
        const shopIndex = resultedCartArray.findIndex(shop => shop.shop === entry.shopId)
        resultedCartArray[shopIndex].products.push(entry)
      } else {
        resultedCartArray.push({ shop: entry.shopId, shopName: entry.shopName, shopPriceMin: entry.shopPriceMin, products: [entry] })
      }
    })

    return resultedCartArray
  }
}
