export default {
  setLocation (state, location) {
    state.location = location
  },
  setStartDate (state, date) {
    state.startDate = date
  },
  setEndDate (state, date) {
    state.endDate = date
  },
  setAdults (state, adults) {
    state.adults = adults
  },
  setChilds (state, childs) {
    state.childs = childs
  },
  setChildsYears (state, years) {
    state.childsYears = years
  }
}
