export default {
  async get ({ commit, rootState }) {
    const params = {
      location: rootState.filter.location.value,
      startDate: rootState.filter.startDate,
      endDate: rootState.filter.endDate,
      adults: rootState.filter.adults,
      childs: rootState.filter.childs,
      childsYears: rootState.filter.childsYears
    }
    await this.$axios.$get('/api/hotels', { params })
      .then((res) => {
        commit('set', res.data)
      })
  },

  show ({ commit }, params) {
    return new Promise((resolve, reject) => {
      this.$axios.get(`api/hotels/${params.id}`)
        .then((res) => {
          commit('setObject', res.data)
          resolve(res)
        }).catch((err) => {
          reject(err)
        })
    })
  }
  // async set ({ commit }, car) {
  //   await commit('set', car)
  // }
}
