export default {
  set (state, posts) {
    state.list = posts
  },
  shuffle (state) {
    state.list = state.list.sort(() => Math.random() - 0.5)
  },
  // add (state, value) {
  //   merge(state.list, value)
  // },
  // remove (state, { car }) {
  //   state.list.splice(state.list.indexOf(car), 1)
  // },
  setObject (state, object) { state.object = object }
}
