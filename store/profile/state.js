export default () => ({
  reservationsList: [],
  productsList: [],
  reviewsList: [],
  dashboardInfo: {},
  inviteEmail: '',
  invitesList: [],
  user: {

  },
  individualOrders: [],
  shopOrders: [],
  availableCouriers: [],
  allOrders: []
})
