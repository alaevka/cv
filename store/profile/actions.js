export default {
  async getDashboardInfo ({ commit, rootState }) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/dashboard')
      .then((res) => {
        commit('setDashboardInfo', res)
      })
  },
  async getReviews ({ commit, rootState }) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/reviews')
      .then((res) => {
        commit('setReviews', res.data)
      })
  },
  async deleteReview ({ commit, rootState }, id) {
    await this.$axios.$delete('/api/user/' + rootState.auth.user.id + '/reviews/' + id)
      .then((res) => {
        commit('setReviews', res.data)
      })
  },

  async getUserInfo ({ commit, rootState }) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/profile')
      .then((res) => {
        commit('setUserInfo', res.data)
      })
  },
  updateUserProfile ({ commit, rootState }) {
    return new Promise((resolve, reject) => {
      this.$axios.put('/api/user/' + rootState.auth.user.id, { profile: rootState.profile.user }).then((response) => {
        resolve(response)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  async deleteProfileMediaItem ({ commit, rootState }, id) {
    await this.$axios.$delete('/api/user/' + rootState.auth.user.id + '/profile/deletemedia/' + id)
      .then((res) => {
        commit('setCurrentProfileMedia', res)
      })
  },
  async getProfileMedia ({ commit, rootState }, id) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/profile/media/' + id)
      .then((res) => {
        commit('setCurrentProfileMedia', res)
      })
  },
  async getIndividualOrders ({ commit, rootState }) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/orders/individual')
      .then((res) => {
        commit('setUserIndividualOrders', res.data)
      })
  },
  async getShopOrders ({ commit, rootState }) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/orders/shop')
      .then((res) => {
        commit('setUserShopOrders', res.data)
      })
  },
  async getAvailableCouriers ({ commit, rootState }, orderId) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/orders/couriers/' + orderId)
      .then((res) => {
        commit('setAvailableCouriers', res.data)
      })
  },
  async getAllOrders ({ commit, rootState }) {
    await this.$axios.$get('/api/user/' + rootState.auth.user.id + '/orders/all')
      .then((res) => {
        commit('setAllOrders', res.data)
      })
  }

}
