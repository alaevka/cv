export default {
  setReservations (state, reservations) {
    state.reservationsList = reservations
  },
  setReviews (state, reviews) {
    state.reviewsList = reviews
  },
  setDashboardInfo (state, info) {
    state.dashboardInfo = info
  },
  setInviteEmail (state, email) {
    state.inviteEmail = email
  },
  setInvites (state, invites) {
    state.invitesList = invites
  },
  setUserFirstName (state, value) {
    state.user.firstName = value
  },
  setUserLastName (state, value) {
    state.user.lastName = value
  },
  setUserSurName (state, value) {
    state.user.surName = value
  },
  setUserPhone (state, value) {
    state.user.phone = value
  },
  setUserEmail (state, value) {
    state.user.email = value
  },
  setUserCar (state, value) {
    state.user.profile.car_name = value
  },
  setUserCarNumber (state, value) {
    state.user.profile.car_number = value
  },
  setUserInfo (state, data) {
    state.user = data
    state.user.subscription = {
      endpoint: '1',
      publicKey: '2',
      authToken: '3',
      contentEncoding: '4'
    }
  },
  setUserCompanyLocation (state, data) {
    state.user.profile.location = data
  },
  setUserCompanyAddress (state, value) {
    state.user.profile.address = value
  },
  setUserCompanyName (state, value) {
    state.user.profile.shop_name = value
  },
  setUserCompanyUrl (state, value) {
    state.user.profile.slug = value
  },
  setUserCompanyDelivery (state, value) {
    state.user.profile.have_own_delivery = value
  },

  setUserCompanyPriceMin (state, value) {
    state.user.profile.priceMin = value
  },
  setUserCompanyAverageTime (state, value) {
    state.user.profile.averageTime = value
  },
  setUserCompanyWorkTimeFrom (state, value) {
    state.user.profile.workTimeFrom = value
  },
  setUserCompanyWorkTimeTo (state, value) {
    state.user.profile.workTimeTo = value
  },
  setCurrentProfileMedia (state, data) {
    state.user.profile.media = data
  },
  setUserIndividualOrders (state, data) {
    state.individualOrders = data
  },
  setUserShopOrders (state, data) {
    state.shopOrders = data
  },
  setAvailableCouriers (state, data) {
    state.availableCouriers = data
  },
  setAllOrders (state, data) {
    state.allOrders = data
  }

}
