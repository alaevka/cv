export default {
  setReviewBody (state, reviewBody) {
    state.review.body = reviewBody
  },
  setReviewCustomerServiceRating (state, rate) {
    state.review.reviewCustomerServiceRating = rate
  },
  setReviewQualityRating (state, rate) {
    state.review.reviewQualityRating = rate
  },
  setReviewFriendlyRating (state, rate) {
    state.review.reviewFriendlyRating = rate
  },
  setReviewPricingRating (state, rate) {
    state.review.reviewPricingRating = rate
  },
  setReviewTotalRating (state, rate) {
    state.review.reviewTotalRating = rate
  },
  setReviewRecommend (state, rate) {
    state.review.reviewRecommend = rate
  },
  setReviews (state, reviews) {
    state.reviews = reviews
  },
  setShopReviews (state, reviews) {
    state.reviews = reviews
  },
  setProductReviews (state, reviews) {
    state.productReviews = reviews
  },
  delReview (state) {
    state.review = {
      body: '',
      reviewCustomerServiceRating: 0,
      reviewQualityRating: 0,
      reviewFriendlyRating: 0,
      reviewPricingRating: 0,
      reviewTotalRating: 0,
      reviewRecommend: 'Yes'
    }
  },
  add (state, value) {
    // console.log(value)
    // merge(state.reviews, value)
  }
}
