export default () => ({
  reviews: [],
  productReviews: [],
  review: {
    body: '',
    reviewCustomerServiceRating: 0,
    reviewQualityRating: 0,
    reviewFriendlyRating: 0,
    reviewPricingRating: 0,
    reviewTotalRating: 0,
    reviewRecommend: 'Yes'
  }
})
