export default {
  async getShopReviews ({ commit, state }, shopId) {
    await this.$axios.$get('/api/shops/' + shopId + '/reviews')
      .then((res) => {
        commit('setShopReviews', res.data)
      })
  },
  async createShopReview ({ commit, state, dispatch }, shopId) {
    await this.$axios.post('/api/shops/' + shopId + '/reviews', state.review).then((res) => {
      dispatch('getShopReviews', shopId)
      commit('delReview')
    })
  },
  async getProductReviews ({ commit, state }, productId) {
    await this.$axios.$get('/api/product/' + productId + '/reviews')
      .then((res) => {
        commit('setProductReviews', res.data)
      })
  },
  async createProductReview ({ commit, state, dispatch }, productId) {
    await this.$axios.post('/api/product/' + productId + '/reviews', state.review).then((res) => {
      dispatch('getProductReviews', productId)
      commit('delReview')
    })
  }
}
