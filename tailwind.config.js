/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    truncate: {
      lines: {
          3: '3',
          4: '4',
          5: '5',
          8: '8',
      }
    },
    fontFamily: {
      'helvetica': ['HelveticaNeueCyr'],
      'helvetica-bold': ['HelveticaNeueCyrBold'],
      'helvetica-heavy': ['HelveticaNeueCyrHeavy'],
      'gteestipro': ['GT Eesti Pro Text'],
    },
    fontWeight: 100,
    maxHeight: {
      '0': '0',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      'full': '100%',
      '64': '16rem',
    },
    extend: {
      width: {
        '26': '26rem',
      }
    }
  },
  purge: false,
  variants: {},
  plugins: [
    require('tailwindcss-truncate-multiline')(),
    require('@tailwindcss/custom-forms'),
    require('tailwindcss-animatecss')({
      classes: ['animate__animated', 'animate__fadeIn', 'animate__bounceIn', 'animate__pulse', 'animate__tada', 'animate__shakeX', 'animate__fadeInUp', 'animate__fadeInDown'],
      settings: {
        animatedSpeed: 1000,
        heartBeatSpeed: 1000,
        hingeSpeed: 2000,
        bounceInSpeed: 750,
        bounceOutSpeed: 750,
        animationDelaySpeed: 1000
      },
      variants: ['responsive', 'hover'],
    }),
  ]
}
