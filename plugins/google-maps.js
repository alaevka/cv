import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCfS8ek0p_BmRea7EJFl2ZsTHew714oBbs',
    libraries: 'places'
  }
})
