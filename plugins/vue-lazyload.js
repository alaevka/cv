import Vue from 'vue'
import VueLazyload from 'vue-lazyload'
import error from '../assets/img/nophoto.svg'

Vue.use(VueLazyload, {
  error
})
