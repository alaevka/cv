import numeral from 'numeral'
import numFormat from 'vue-filter-number-format'
import Vue from 'vue'

if (numeral.locales['rf-en-spaces'] === undefined) {
  numeral.register('locale', 'rf-en-spaces', {
    delimiters: {
      thousands: ' ',
      decimal: '.'
    },
    abbreviations: {
      thousand: 'k',
      million: 'm',
      billion: 'b',
      trillion: 't'
    },
    ordinal (number) {
      const b = number % 10
      return (~~(number % 100 / 10) === 1) ? 'th'
        : (b === 1) ? 'st'
          : (b === 2) ? 'nd'
            : (b === 3) ? 'rd' : 'th'
    },
    currency: {
      symbol: '₽'
    }
  })
}
numeral.locale('rf-en-spaces')

Vue.filter('numFormat', numFormat(numeral))
