import Vue from 'vue'
import { Bar, Line } from 'vue-chartjs'

Vue.component('Bar', Bar)
Vue.component('Line', Line)
