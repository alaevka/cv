import { HorizontalBar } from 'vue-chartjs'

export default {
  extends: HorizontalBar,
  props: {
    data: {
      type: Array,
      default: []
    },
    labels: {
      type: Array,
      default: []
    }
  },
  mounted () {
    this.renderChart({

      labels: this.labels,
      datasets: [
        {

          label: 'Баланс нутриенотов в процентном соотношении',
          backgroundColor: '#8dce9e',
          data: this.data
        }
      ]
    }, { responsive: true, maintainAspectRatio: false })
  }
}
