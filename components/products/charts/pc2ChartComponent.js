import { Pie } from 'vue-chartjs'

export default {
  extends: Pie,

  props: {
    data: {
      type: Array,
      default: []
    }
  },
  mounted () {
    this.renderChart({
      labels: ['Белки', 'Жиры', 'Углеводы'],
      datasets: [
        {
          backgroundColor: [
            '#E2E8F0',
            '#FEB2B2',
            '#9AE6B4'
          ],
          data: this.data
        }
      ]
    }, { responsive: true, maintainAspectRatio: false, legend: { position: 'left' } })
  }
}
