require('dotenv').config()
import axios from 'axios'

export default {
  //target: 'static',
  ssr: true,
  env: {
    api_url: process.env.API_URL
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'title',
    meta: [
      // { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover' },
      { 
        hid: 'description', 
        name: 'description', 
        content: 'description' 
      },
      { 
        hid: 'keywords', 
        name: 'keywords', 
        content: 'keywords' 
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.svg' }
    ],
    bodyAttrs: {
      class: 'bg-white h-full font-gteestipro antialiased swal2-toast-shown swal2-shown'
    } 
  },
  router: {
    middleware: ['auth'],
    // extendRoutes (routes, resolve) {
      

      
    // } 
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#8dce9e' },
  /*
  ** Global CSS
  */
  css: [
    '~assets/webfonts/helveticaNeue.css',
    '~assets/webfonts/gteestipro/gteestipro.css',
    '~assets/css/transitions.css',
    'sweetalert2/dist/sweetalert2.css',
    'vue-multiselect/dist/vue-multiselect.min.css',
    'epic-spinners/dist/lib/epic-spinners.min.css',
    '~assets/css/app.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~plugins/vue-progressive-image', ssr:false },
    { src: "~/plugins/google-maps", ssr: false },
    { src: '~/plugins/datepicker', ssr: false },
    { src: '~/plugins/star-rating', ssr: false },
    { src: '~/plugins/vue-js-modal', ssr: false },
    { src: '~/plugins/filter-number', ssr: true },
    { src: '~/plugins/vue-lazyload', ssr: true },
    { src: '~/plugins/count-down-timer', ssr: false },
    { src: '~/plugins/vue-carousel', ssr: false },
    { src: '~/plugins/vue-debounce', ssr: false },
    { src: '~/plugins/vue-masonry', ssr: false },
    { src: '~/plugins/vue-paginate', ssr: false },
    { src: '~/plugins/event-bus', ssr: false },
    // { src: '~/plugins/jivosite', ssr: false },
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/moment',
    '@nuxtjs/pwa',
    ['@nuxtjs/google-analytics', {
      id: 'id'
    }]
  ],
  moment: {
    defaultLocale: 'ru',
    locales: ['ru']
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    'cookie-universal-nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxtjs/auth',
    'vue-sweetalert2/nuxt',
    'vue-wait/nuxt',
    '@nuxtjs/laravel-echo',
    'vue-social-sharing/nuxt',
    //['nuxt-canonical', { baseUrl: 'https://rf.market' }],
    ['nuxt-fontawesome', {
      component: 'fa', 
      imports: [
        //import whole set
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['fas']
        },
        {
          set: '@fortawesome/free-brands-svg-icons',
          icons: ['fab']
        },
      ]
    }],
    ['vue-scrollto/nuxt', { duration: 500 }],
    [
      '@rkaliev/nuxtjs-yandex-metrika',
      {
        id: 'id',
        webvisor: true,
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        trackHash: true,
        ecommerce: 'dataLayer',
        debug: false,
        noJS: false,
      },
    ],
    // [
    //   '@nuxtjs/yandex-metrika',
    //   {
    //     id: '65955469',
    //     webvisor: true,
    //     // clickmap:true,
    //     // useCDN:false,
    //     // trackLinks:true,
    //     // accurateTrackBounce:true,
    //   }
    // ],
    ['@nuxtjs/robots', { UserAgent: '*', Host: 'https://host', Disallow: ['/profile', '/auth/verify', '/rebuild'] }],
    '@nuxtjs/sitemap',
    ['@nuxtjs/google-gtag', { 
      id: 'id',
    }]
  ],
  pwa: {
    workbox: false,
    icon: {
      source: 'static/icon.png',
      filename: 'icon.png?v1'
    }
  },
  sitemap: {
    // hostname: 'https://rf.market',
    // gzip: true,
    // exclude: [
    //   '/cart/fault',
    //   '/cart/paid',
    //   '/cart/checkout',
    //   '/index.old',
    //   '/index2.old',
    //   '/auth/verify',
    //   '/profile/**',
    //   '/rebuild'
    // ],
    // routes: async () => {
    //   const api = await Prismic.getApi("https://timbenniks.prismic.io/api/v2")
    //   const writings = await api.query(Prismic.Predicates.at("document.type", "writing"))
    //   return writings.results.map(writing => `/writings/${writing.uid}`)
    // },
    path: '/sitemap.xml',
    hostname: 'https://host',
    cacheTime: 1000 * 60 * 15,
    gzip: true,
    sitemaps: [
      {
        path: '/sitemap/sitemap.xml',
        exclude: [
          '/cart/fault',
          '/cart/paid',
          '/cart/checkout',
          '/index.old',
          '/index2.old',
          '/auth/verify',
          '/profile/**',
          '/rebuild'
        ],
        defaults: {
          changefreq: 'daily',
          priority: 1,
          lastmod: new Date()
        }
      },
      {
        path: '/sitemap/categories/sitemap.xml',
        exclude: [],
        routes: async () => {
          let apiUrl = process.env.API_URL+'/api/sitemap?type=1'
          const { data } = await axios.get(`${apiUrl}`)
          return data.map(v => `/${v.id}`)
        },
        defaults: {
          changefreq: 'daily',
          priority: 1,
          lastmod: new Date()
        }
      },
      {
        path: '/sitemap/products/sitemap.xml',
        exclude: [],
        routes: async () => {
          let apiUrl = process.env.API_URL+'/api/sitemap?type=2'
          const { data } = await axios.get(`${apiUrl}`)
          return data.map(v => `/${v.id}`)
        },
        defaults: {
          changefreq: 'daily',
          priority: 1,
          lastmod: new Date()
        }
      },
    ]
  },
  auth: {
    resetOnError: true,
    // Options
    redirect: {
      login: '/auth/login'
    },
    strategies: {
      local: {
        endpoints: {
          login: { url: '/api/verify', method: 'post', propertyName: 'success.token' },
          logout: { url: '/api/logout', method: 'post' },
          user: { url: '/api/user', method: 'get', propertyName: 'data' }
        }
      }
    },
    tokenRequired: true,
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  wait: { useVuex: true },
  axios: {
    baseURL: process.env.API_URL
  },
  // oneSignal: {
  //   init: {
  //     appId: '8eab9c89-81c0-4133-a55e-84c0c73956b4',
  //     allowLocalhostAsSecureOrigin: true,
  //     welcomeNotification: {
  //         disable: true
  //     }
  //   }
  // },
  // echo: {
  //   //broadcaster: 'socket.io',
  //   //host: window.location.hostname + ':6001'
  //   broadcaster: 'pusher',
  //   key: 'b5987f9444f674935454',
  //   cluster: 'eu',
  //   forceTLS: true,
  //   authModule: true,
  //   connectOnLogin: true,
  //   disconnectOnLogout: true,
  //   authEndpoint: 'api/user'
  // },
  purgeCSS: {
    // whitelistPatterns: [/(^|\.)fa-/, /-fa($|\.)/]
    enabled: false
  },
  
  /*
  ** Build configuration
  */
  build: {
    transpile: [
      "vee-validate/dist/rules"
    ],
    // extractCSS: {
    //   ignoreOrder: true
    // },
    // optimization: {
    //   minimize: true,
    //   splitChunks: {
    //     chunks: 'all',
    //     maxSize: 249856,
    //     cacheGroups: {
    //       styles: {
    //         name: 'styles',
    //         test: /\.(css|vue)$/,
    //         chunks: 'all',
    //         enforce: true
    //       }
    //     }
    //   }
    // },
    // analyze:true,
    extend (config, ctx) {
      
      if (ctx.isDev && ctx.isClient) {
        
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    }
  }
}
