export function filterProducts (filter, products) {
  let filteredList = [...products]

  // Search
  // if (filter.search !== '') {
  //   const searchList = []
  //   const searchTerm = filter.search.toLowerCase()
  //   for (let i = 0; i < filteredList.length; i++) {
  //     if (
  //       (filteredList[i].companyName !== null && filteredList[i].companyName.toLowerCase().includes(searchTerm)) ||
  //       (filteredList[i].jobTitle !== null && filteredList[i].jobTitle.toLowerCase().includes(searchTerm))
  //     ) {
  //       searchList.push(filteredList[i])
  //     }
  //   }
  //   filteredList = searchList
  // }

  let searchList = []

  if (filter.searchByName !== '') {
    const searchByNameTerm = filter.searchByName.toLowerCase()

    searchList = filteredList.filter(function (v, i) {
      return (v.name.toLowerCase().includes(searchByNameTerm))
    })

    filteredList = searchList
  }

  if (filter.searchByPriceMin !== '') {
    const searchByPriceMinTerm = parseFloat(filter.searchByPriceMin)

    searchList = filteredList.filter(function (v, i) {
      return (v.price >= searchByPriceMinTerm)
    })

    filteredList = searchList
  }

  if (filter.searchByPriceMax !== '') {
    const searchByPriceMaxTerm = parseFloat(filter.searchByPriceMax)

    searchList = filteredList.filter(function (v, i) {
      return (v.price <= searchByPriceMaxTerm)
    })

    filteredList = searchList
  }

  return filteredList
}
