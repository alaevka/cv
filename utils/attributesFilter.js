export function filterAttributes (filter, attributes) {
  let filteredList = [...attributes]

  // Search
  if (filter.name !== '') {
    const searchList = []
    const searchTerm = filter.name.toLowerCase()
    for (let i = 0; i < filteredList.length; i++) {
      if (
        (filteredList[i].name !== null && filteredList[i].name.toLowerCase().includes(searchTerm))
      ) {
        searchList.push(filteredList[i])
      }
    }
    filteredList = searchList
  }

  return filteredList
}
