export const getUserFromCookie = (req) => {
  if (!req.headers.cookie) { return }
  const jwtCookie = req.headers.cookie.split(';').find(c => c.trim().startsWith('jwt='))
  if (!jwtCookie) { return }
  const jwt = jwtCookie.split('=')[1]
  return jwt
}

export const getUserFromLocalStorage = () => {
  const json = window.localStorage.user
  return json ? JSON.parse(json) : undefined
}
