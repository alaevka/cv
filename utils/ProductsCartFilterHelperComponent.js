export function filterProductsByShop (order, cartProducts) {
  const cartProductsSorted = cartProducts.sort(function (a, b) {
    return ((a.product.shopId === b.product.shopId) ? 0 : ((a.product.shopId > b.product.shopId) ? 1 : -1))
  })
  return cartProductsSorted
}
